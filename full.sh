#!/bin/sh

dir=`dirname $0`

song="mGgMZpGYiy8"
if [ $# -gt 0 ] ; then
  song=$1
fi

mpc clear
$dir/main.js $song | mpc add
mpc play
$dir/main.js list 50 $song | parallel "mpc add {}"
