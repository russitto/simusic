#!/usr/bin/env node

'use strict'

const { format } = require('util')

const miniget = require('miniget')

const config = require('./config.json')

const l = console.log.bind(console)
const e = console.error.bind(console)
const URL = 'https://youtube.googleapis.com/youtube/v3/search?maxResults=%d&part=snippet&key=%s&q=%s'

module.exports = {
  search,
}

if (process.argv.length > 1 && process.argv[1] === __filename) {
  main()
}
async function main() {
  if (process.argv.length < 4) {
    l(`There is no you, there is no argument… Example:
      > node search.js 10 joy division`)
    process.exit(1)
  }
  process.argv.shift()
  process.argv.shift()

  let maxResults = parseInt(process.argv[0]) || undefined
  if (isNaN(maxResults)) {
    maxResults = undefined
  } else {
    process.argv.shift()
  }
  const q = process.argv.join('+')
  const items = await search(q, maxResults) || process.exit(2)
  printItems(items)
}

async function search(q, maxResults = 5) {
  const url = format(URL, maxResults, config.api_key, q)
  let body
  try {
    body = await miniget(url).text()
  } catch(err) {
    e(err)
    return false
  }
  const data = JSON.parse(body)
  const items = data.items.map((item) => { 
    return {
      id: item.id.videoId,
      title: item.snippet.title,
      thumb: item.snippet.thumbnails.default.url,
    }
  })
  return items
}

function printItems(items) {
  items.forEach((item) => {
    l(item.id, item.title, item.thumb)
  })
}
