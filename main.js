#!/usr/bin/env node

'use strict'

const { existsSync } = require('fs')
const { format } = require('util')

const ytdl = require('ytdl-core')
const cookie = require('cookies.txt')
const miniget = require('miniget')

const config = require('./config.json')

const l = console.log.bind(console)
const e = console.error.bind(console)

const LIST_URL = 'https://youtube.googleapis.com/youtube/v3/playlistItems?maxResults=%d&part=contentDetails&key=%s&playlistId=%s'

let cookieString = false
let requestOptions = {}

module.exports = {
  getOne,
  getList,
}

if (process.argv.length > 1 && process.argv[1] === __filename) {
  main()
}
async function main() {
  if (process.argv.length < 3) {
    l(`There is no you, there is no argument… Examples:
      a song   > node main.js gKno6Dp3rw4
      a list   > node main.js list RDaadf1323184`)
    process.exit(1)
  }

  const length = process.argv.length
  const id = process.argv.pop()
  cookieString = await parseCookie()
  if (cookieString) {
    requestOptions = { 
      headers: {
        Cookie: cookieString,
        // 'x-youtube-client-name': '1',
        // 'x-youtube-client-version': '2.20191008.04.01'
      }
    }
  }

  if (length === 3) {
    const one = await getOne(id)
    l(one)
    return
  }

  if (process.argv[2] === 'list') {
    const size = process.argv.pop()
    const list = await getList(id, parseInt(size) || 20)
    let proms = []
    for (let i = 0, len = list.length; i < len; i++) {
      proms.push(getOne(list[i])
        .then((url) => l(url))
        .catch((err) => e(err))
      )
    }
  }
}

async function getOne(id) {
  e('-- DOING get one', id)
  const info = await ytdl.getInfo(id, { requestOptions })
  e('-- DONE get one', id)
  const format = ytdl.chooseFormat(info.formats, { quality: 'highestaudio' })
  const duration = toMinutes(info.videoDetails.lengthSeconds)
  let title = info.videoDetails.title.replace(/\s/g, '_')
  if (duration) {
    title += '_(' + duration + ')'
  }
  return format.url + '&_' + title + ';;;' + id
}

function getList(id, size = 20) {
  // related songs:
  if (id.length < 12) {
    id = 'RD' + id
  }
  if (size > 50) size = 50
  if (size < 1)  size = 20
  const url = format(LIST_URL, size, config.api_key, id)

  e('-- DOING get list')
  return miniget(url).text()
  .then((data) => {
    e('-- DONE get list')
    const list = JSON.parse(data)
    return list.items.map((vid) => vid.contentDetails.videoId)
  })
}

function toMinutes(seconds) {
  if (!seconds) return seconds
  seconds = parseInt(seconds)
  const secs = seconds % 60
  const mins = parseInt(seconds / 60)
  return `${mins}:${secs}`
}

function parseCookie() {
  const fpath = __dirname + '/cookies.txt'
  if (!existsSync(fpath)) {
    return false
  }

  return new Promise((resolve, reject) => {
    cookie.parse(fpath, (cookies) => {
      const result = cookies.reduce(function (pre, cookie) {
        pre.push(cookie.name + '=' + cookie.value)
        return pre
      }, [])
      resolve(result.join('; '))
    })
  })
}
