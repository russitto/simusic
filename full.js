#!/usr/bin/env node

'use strict'

const rl = require('readline').createInterface({
  input: process.stdin,
  output: process.stdout,
})

const mpd = require('mpd2')

const config = require('./config.json')
const { search } = require('./search')
const { getOne, getList } = require('./main')

const l = console.log.bind(console)
const e = console.error.bind(console)

main()
async function main() {
  if (process.argv.length < 3) {
    l(`There is no you, there is no argument… Examples:
      a song   > node full.js joy division`)
    process.exit(1)
  }
  const client = await mpd.connect(config.mpris.mpd)
  process.argv.shift()
  process.argv.shift()

  const max = 10
  const items = await search(process.argv.join('+'), max)
  printItems(items)
  const sel = await getInt(max)
  const id = items[sel].id

  const first = await getOne(id)
  await client.sendCommand('clear')
  await client.sendCommand(mpd.cmd('add', first))
  await client.sendCommand('play')
  // END first one

  const list = await getList(id, 50)
  const proms = list.map(async (song) => {
    const s = await getOne(song)
    return client.sendCommand(mpd.cmd('add', s))
  })
  await Promise.all(proms)

  await client.disconnect()
  process.exit(0)
}

function printItems(items) {
  items.forEach((item, i) => {
    l(i, item.id, item.title, item.thumb)
  })
}

function getInt(max) {
  const question = `-->[0, ${max - 1}]: `
  l('doing q')
  return new Promise((resolve) => {
    let thaNum = -1
    rl.question(question, (num) => {
      thaNum = Number(num)
      if (thaNum >= 0 && thaNum < max) {
        resolve(thaNum)
      }
      resolve(0)
    })
  })
}
