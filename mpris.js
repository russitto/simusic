#!/usr/bin/env node

'use strict'

const { writeFile } = require('fs')
const { format } = require('util')

const service = require('mpris-service')
const mpd = require('mpd2')
const miniget = require('miniget')

const config = require('./config.json')

const l = console.log.bind(console)
const e = console.error.bind(console)

const URL = 'https://www.googleapis.com/youtube/v3/videos?id=%s&part=snippet&key=%s'
const statusMap = {
  play: 'Playing',
  pause: 'Paused',
  stop: 'Stopped',
}

const player = service({
	name: 'nodejs',
	identity: 'simusic media player',
	supportedUriSchemes: ['file', 'http'],
	supportedMimeTypes: ['audio/mpeg', 'application/ogg'],
	supportedInterfaces: ['player'],
})

main()
async function main() {
  writeFile(__dirname + '/mpris.pid', ''+process.pid, () => {})

  const client = await mpd.connect(config.mpris.mpd)
  client.sendCommand('status').then((data) => {
    // l(mpd.parseObject(data))
    sendInfo()
  })
  client.on('system', (name) => {
    if (name === 'player') {
      sendInfo()
    }
  })

  const events = ['play', 'pause', 'stop', 'next', 'previous']
  events.forEach((event) => {
    player.on(event, () => client.sendCommand(event))
  })

  async function sendInfo() {
    const song = await getSong() 
    if (!song) {
      return false
    }
    player.metadata = {
      'mpris:artUrl': song.thumb,
    	'xesam:title': song.title,
    }

    player.playbackStatus = song.state
  }

  async function getSong() {
    const [ status, playlist ] = await sendCommands(['status', 'playlist'])
    const state = statusMap[status.state]
    if (typeof status.song === 'undefined' || status.song >= playlist.length) {
      return false
    }
    const url = playlist[status.song]
    const id = url.replace(/.*;;;/, '')
    const apiUrl = format(URL, id, config.api_key)
    const body = await miniget(apiUrl).text()
    const video = JSON.parse(body)
    if (video.items.length === 0) {
      return {
        title: url,
        thumb: '',
        state,
      }
    }
    return {
      title: video.items[0].snippet.title,
      thumb: getThumb(video.items[0].snippet.thumbnails),
      state,
    }
  }

  async function sendCommands(commands) {
    const values = await Promise.all(commands.map((command) => client.sendCommand(command)))
    return values.map((value, i) => {
      if (commands[i] === 'playlist') {
        const songs = value.trim().split('\n')
          .filter((song) => song !== 'OK')
          .map((song) => {
          const parsed = mpd.parseObject(song)
          return parsed.___file || parsed.__file
        })
        return songs
      }
      return mpd.parseObject(value)
    })
  }
}

function getThumb(thumbs) {
  if (thumbs.standard) {
    return thumbs.standard.url
  } 
  if (thumbs.high) {
    return thumbs.high.url
  } 
  return thumbs.default.url
}
